mod points;
pub use crate::points::Point;

mod vectors;
pub use crate::vectors::{cross, dot, Vector};

/// Naive floating point comparisons can be hardware-specific. This epsilon is a global
/// tolerance for equivalence between two floating point values.
pub const EPSILON: f64 = 0.00001;

/// Returns whether two numbers are roughly equal, within the global tolerance.
pub fn eq_with_tolerance(a: f64, b: f64) -> bool {
    (a - b).abs() < EPSILON
}

#[cfg(test)]
mod tests {
    use super::*;
    /// Page 5: we need a rough equals
    #[test]
    fn test_rough_equals() {
        assert_eq!(eq_with_tolerance(1.0000000001, 1.0000000002), true);
        assert_eq!(eq_with_tolerance(1.0000000001, 1.0000100001), false);
    }
}
