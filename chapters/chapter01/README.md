# Chapter 1

## Tuples

- Book uses left-handed coordinates. Apparently so does Unity and POV-Ray.
- Book wants to use 4-tuples `(x, y, z, w)` embedded in 3-space, with the 4th element `w` indicating vector (`w=0`) or point (`w=1`).
- Points, vectors etc.
  - The book suggests implementing points and vectors as the same type (`tuple`) but with
    a different value (`w`). I think it's more idiomatic to just use Rust's type system, 
    so will implement them as separate types.
- **Rust details:**
  - By using `///` docstrings, a `cargo doc` will generate nice looking docs for everything.
    Using the `--open` flag will open it in the browser automatically.
  - Comparing the structs themselves will require inheriting traits (e.g. PartialEq) but that requires some more implementation. Probably isn't needed just yet.

## Operations

- The book is building up pointer arithmetric:
  - Vectors can operate on each other (associatively), vectors can operate on points, but points cannot operate on points.
  - Vector addition, subtraction. Scalar multiplication, division. Magnitudes, normalization, unit vectors. Dot products, cross products, etc.
    - It would be really nice to generalize this to N-d, not mathematically difficult I think. But it would probably be an overcomplication.
- **Rust details:**
  - I think these can all be done easily as methods for the respective structs. In Python it would be easily possible to overload the operators (e.g. `vector + point --> new point`). How to do this in Rust?
  - So after implementing some of the basic arithmetric operations I indeed hit an issue on overloading.
    - https://doc.rust-lang.org/rust-by-example/trait/ops.html seems to be one approach. The idea being I want to implement the trait Add/Sub etc for a particular type.
    - The "Traits" chapter in *Programming Rust* is helpful here, and it seems like a lot of this can be cleaned up with traits.

## Putting it all together

- Now that there are points and vectors is does a little projectile calculator.
  - Iterative calculation of position/velocity, neat.
  - I think it's only first order though?
    - `x_1 = x_0 + dx_0` updates position based on current velocity only
    - `dx_1 = dx_0 + ddx_gravity + ddx_wind` updates velocity based on constant accelerations
- Can run the solution with `cargo run --example projectile`