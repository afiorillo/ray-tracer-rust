/// Actually, this is just the image crate.
/// <https://docs.rs/image/latest/image/struct.ImageBuffer.html> has examples
/// 
/// The book's canvas is just an `RgbImage` buffer.
/// It already supports putting pixels, also doing so with a generator function.
/// It also supports a `.save("path.ext")` method, which itself can support various types.
pub struct Nothing {}

#[cfg(test)]
mod tests {
    #[test]
    fn test_it_works() {
        println!("it works!")
    }
}
