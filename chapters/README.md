# Chapters

Follows along the same chapter structure as the book.
As a reminder, each chapter should be a separate cargo package, i.e.

```bash
$ cd chapters
$ cargo new chapterN --lib
```

## Setup

This is structured like a cargo workspace.
It makes generating docs and running tests across all chapters a bit easier.
For example:

**To run all the tests:**

```bash
$ cargo test --all
```

**To generate the docs (not including dependencies):**

```bash
$ cargo doc --open --no-deps --all
```
