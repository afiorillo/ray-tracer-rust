use chapter01::{Point, Vector};
use image::imageops::flip_vertical;
use image::{Rgb, RgbImage};

#[derive(Copy, Clone)]
struct WorldPhysics {
    pub gravity: Vector,
    pub wind: Vector,
}

struct Projectile {
    pub position: Point,
    pub velocity: Vector,
    pub color: Rgb<u8>,
}

fn tick(env: WorldPhysics, proj: Projectile) -> Projectile {
    // Increments the position of the projectile within an infinitesimal amount of time
    let position = proj.position + proj.velocity;
    let velocity = proj.velocity + env.gravity + env.wind;
    Projectile {
        position,
        velocity,
        color: proj.color,
    }
}

fn draw_projectile(canvas: &mut RgbImage, proj: &Projectile) {
    // interestingly, this wraps!
    // so if the position exceeds the bounds, it just wraps around
    canvas.put_pixel(
        proj.position.x.ceil() as u32,
        proj.position.y.ceil() as u32,
        proj.color,
    )
}

/// Simulates launching an object from a canon, with gravity and wind.    
fn main() {
    // note that the origin is in the top-left, and positive y-axis is "down" on the image
    let mut canvas = RgbImage::new(900, 550);

    // Start the projectile one unit above the origin, and moving with a norm'd velocity
    let mut proj = Projectile {
        position: Point::new(0.0, 1.0, 0.0),
        velocity: Vector::new(0.5, 1.0, 0.0).normalized() * 11.25,
        color: Rgb([255, 0, 0]),
    };
    // Set up the environmental factors
    let env = WorldPhysics {
        gravity: Vector::new(0.0, -0.1, 0.0),
        wind: Vector::new(-0.02, -0.005, 0.0),
    };

    // And now we iteratively update the state of the world until the projectile is
    // (approximately) at y=0, i.e. fallen.
    while proj.position.y > 0.0 {
        draw_projectile(&mut canvas, &proj);
        proj = tick(env, proj);
    }

    // And now we should write the canvas out to a file. We will flip it before hand so
    // the coordinates match our expectations
    canvas = flip_vertical(&canvas);
    canvas
        .save("projectile.png")
        .expect("unable to write canvas to file");
}
