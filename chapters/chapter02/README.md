# Chapter 2: Drawing on a Canvas

## Colors

- The book suggests an RGB implementation with floats as a 3-tuple. `(red, green, blue)`.

## Color Operations

- Adding colors (like adding a vector)
- Subtracting colors (like subtracting a vector)
- Scalar multiplication (same as a vector)
- Color multiplication. They call it the Hadamard product, but if these were vectors it would be the dot product.

## Canvas

- A canvas is a rectangular grid of pixels, each represented by a color.
- The book assumes 0-based indexes and gives a signature like `write_pixel(canvas, x, y, color)`.

## Writing to a canvas

- Uses the PPM file format for images. I'll use [geeqie](https://www.geeqie.org/help/GuideReferenceSupportedFormats.html) to view these.
- The format begins with a header like

```
P3
80 40
255
```

- The "P3" is the magic number for this PPM syntax.
  The "80 40" gives the width and height of the canvas.
  The "255" gives the maximum color value. I.e. 24-bit RGB. 
- The contents follow: each row is on a separate line; each pixel is represented `red green blue`, space delimited; and each pixel in a column is space delimited.
- So, for example a 2x2 image where the top-left is red and bottom-right is green looks like

```PPM
P3
2 2
255
255 0 0 0 0 0
0 0 0 0 255 0
```

## Putting it all together

- Re-do the projectile example, but now drawing its position on the canvas.

## Doing it my way

- So the super-basic color implementation is too basic I think, and I know that there are existing Rust crates that do this kind of color mixing better.
- Also PPM isn't rendered by Gitlab, which would be nice. I prefer using a more common image format like PNG, but this means another crate.
- Ultimately I think the best option is to implement the canvas idea, but using external crates for colors and PNG writing.
- IIRC the Programming Rust book covers an example very similar to this, but parallelized. That would be nice to put in here.
- Running `cargo run --example projectile` writes a `projectile.png` with the output.

The result looks like

![projectile example result](projectile.png)
