/// This is a fake structure really just used as an anchor for more docs.
/// It's likely that, if you're reading this, you are either lost or visited via the hosted
/// crate documentation. If the latter, click on each of the chapter crates on the side-bar
/// for more documentation. If the former, good luck!
pub struct Nothing {}
