#!/usr/bin/env bash

TARGET_DIR="$(git rev-parse --show-toplevel)/target/"
DOCS_DIR="$(git rev-parse --show-toplevel)/public/"

# actually generate the docs
cargo doc --no-deps --all --target-dir="$TARGET_DIR" --manifest-path=chapters/Cargo.toml

# and then move them to the public directory for Gitlab pages, and cleanup
mv "$TARGET_DIR/doc" "$DOCS_DIR"
rm -rf "$TARGET_DIR"
