# The Ray Tracer Challenge, in Rust

This repository is my follow-along notes and exercises from [The Ray Tracer Challenge](https://pragprog.com/titles/jbtracer/the-ray-tracer-challenge/).
All exercises are implemented in Rust.

## Setup

Ensure [rustup](https://rustup.rs/) has been installed.

```bash
$ cargo version
cargo 1.62.1 (a748cf5a3 2022-06-08)
$ rustc --version
rustc 1.62.1 (e092d0b6b 2022-07-16)
```
