use crate::eq_with_tolerance;
use crate::vectors::Vector;
use std::fmt;
use std::ops::{Add, Neg, Sub};

/// A point represents an infinitesimally small region of (3-)space.
/// Canonically, this library uses a left-handed coordinate system.
/// <https://www.evl.uic.edu/ralph/508S98/coordinates.html> has a picture.
#[derive(Debug)]
pub struct Point {
    /// The distance from the origin in the x-direction.
    pub x: f64,
    /// The distance from the origin in the y-direction.
    pub y: f64,
    /// The distance from the origin in the z-direction.
    pub z: f64,
}

impl Point {
    /// Constructs a new point with the given values
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }
}

impl PartialEq for Point {
    fn eq(self: &Self, other: &Self) -> bool {
        eq_with_tolerance(self.x, other.x)
            && eq_with_tolerance(self.y, other.y)
            && eq_with_tolerance(self.z, other.z)
    }
}

impl Add<Vector> for Point {
    type Output = Point;

    /// Constructs a point by going from this point `self` along the given vector
    fn add(self, vec: Vector) -> Point {
        Point {
            x: self.x + vec.x,
            y: self.y + vec.y,
            z: self.z + vec.z,
        }
    }
}

impl Sub<Point> for Point {
    type Output = Vector;

    /// Constructs a vector going from the origin to this point `self`
    fn sub(self, origin: Point) -> Vector {
        Vector::new(self.x - origin.x, self.y - origin.y, self.z - origin.z)
    }
}

impl Sub<Vector> for Point {
    type Output = Point;

    /// Constructs the point that, if added to the given vector, would result in this point
    fn sub(self, vec: Vector) -> Point {
        Point {
            x: self.x - vec.x,
            y: self.y - vec.y,
            z: self.z - vec.z,
        }
    }
}

impl Neg for Point {
    type Output = Point;

    /// Constructs a point that is the mirror of this one, from the origin
    fn neg(self) -> Point {
        Point {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "x: {0}, y: {1}, z: {2}",
            format!("{0:.5}", self.x),
            format!("{0:.5}", self.y),
            format!("{0:.5}", self.z),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Page 4: we need to be able to construct points
    #[test]
    fn test_point_factory() {
        let p = Point::new(4.0, -4.0, 3.0);

        assert_eq!(p.x, 4.0);
        assert_eq!(p.y, -4.0);
        assert_eq!(p.z, 3.0);
    }

    /// Page 6: Subtracting two points should give the vector connecting them
    #[test]
    fn test_vector_from_two_points() {
        let p1 = Point::new(3.0, 2.0, 1.0);
        let p2 = Point::new(5.0, 6.0, 7.0);
        // the vector that points from p2 to p1
        let v12 = p1.sub(p2);

        assert_eq!(v12.x, -2.0);
        assert_eq!(v12.y, -4.0);
        assert_eq!(v12.z, -6.0);
    }

    #[test]
    fn test_point_equality() {
        let p1 = Point::new(1.0, 2.0, 3.0);
        let p2 = Point::new(1.0, 2.0, 3.0);

        assert_eq!(p1 == p2, true);
    }

    #[test]
    fn test_point_negation() {
        let p1 = Point::new(1.0, 2.0, 3.0);
        assert_eq!(-p1 == Point::new(-1.0, -2.0, -3.0), true);
    }
}
