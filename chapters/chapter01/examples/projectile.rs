use chapter01::{Point, Vector};
use std::thread::sleep;
use std::time::Duration;

#[derive(Copy, Clone)]
struct WorldPhysics {
    pub gravity: Vector,
    pub wind: Vector,
}

struct Projectile {
    pub position: Point,
    pub velocity: Vector,
}

fn tick(env: WorldPhysics, proj: Projectile) -> Projectile {
    // Increments the position of the projectile within an infinitesimal amount of time
    let position = proj.position + proj.velocity;
    let velocity = proj.velocity + env.gravity + env.wind;
    Projectile { position, velocity }
}

/// Simulates launching an object from a canon, with gravity and wind.    
fn main() {
    // Start the projectile one unit above the origin, and moving with a norm'd velocity
    let mut proj = Projectile {
        position: Point::new(0.0, 1.0, 0.0),
        velocity: Vector::new(0.5, 1.0, 0.0).normalized(),
    };
    // Set up the environmental factors
    let env = WorldPhysics {
        gravity: Vector::new(0.0, -0.1, 0.0),
        wind: Vector::new(-0.01, 0.0, 0.0),
    };

    // And now we iteratively update the state of the world until the projectile is
    // (approximately) at y=0, i.e. fallen.
    while proj.position.y > 0.0 {
        println!("position is {}", proj.position);
        proj = tick(env, proj);
        sleep(Duration::from_millis(100));
    }
}
