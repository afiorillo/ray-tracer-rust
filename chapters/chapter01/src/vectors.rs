use crate::eq_with_tolerance;
// Sneaky! If this use is omitted (and Vector doesn't use it), then this still fails to
// compile because the test involving point/vector subtraction lives here and uses the
// implementation from Point. That's only available if the trait is in scope, i.e. used
// here.
use std::ops::{Add, Div, Mul, Neg, Sub};

/// A vector represents a *change* in points within (3-) space.
/// This is implicitly expressed with some point in space, and the vector is the line
/// drawn from the origin `(0,0,0)` to that point.
/// Importantly, this is not a Rust vector (e.g. `vec!` macro), but a mathematical vector.
#[derive(Debug, Copy, Clone)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector {
    /// Constructs a new vector that goes from the origin to the given point positions
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }

    /// Returns the Euclidean magnitude of the vector -- its length.
    /// Incidentally, this is equivalent to the square root of the dot product of a vector
    /// with itself.
    pub fn magnitude(self) -> f64 {
        return (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt();
    }

    /// Constructs a new vector with the same direction as this one, but with magnitude
    /// 1. I.e. constructs a normalized vector with the same direction.
    pub fn normalized(self) -> Vector {
        let mag = self.magnitude();
        Vector {
            x: self.x / mag,
            y: self.y / mag,
            z: self.z / mag,
        }
    }

    pub const UNIT_X: Vector = Vector {
        x: 1.0,
        y: 0.0,
        z: 0.0,
    };
    pub const UNIT_Y: Vector = Vector {
        x: 0.0,
        y: 1.0,
        z: 0.0,
    };
    pub const UNIT_Z: Vector = Vector {
        x: 0.0,
        y: 0.0,
        z: 1.0,
    };
}

impl PartialEq for Vector {
    fn eq(self: &Self, other: &Self) -> bool {
        eq_with_tolerance(self.x, other.x)
            && eq_with_tolerance(self.y, other.y)
            && eq_with_tolerance(self.z, other.z)
    }
}

impl Add<Vector> for Vector {
    type Output = Vector;

    /// Returns a new vector that is the result of vector addition between self and the
    /// given vector.
    fn add(self, vec: Vector) -> Vector {
        Vector {
            x: self.x + vec.x,
            y: self.y + vec.y,
            z: self.z + vec.z,
        }
    }
}

impl Sub<Vector> for Vector {
    type Output = Vector;

    /// Returns a new vector that is the result of vector subtraction between self and the
    /// given vector. In particular, this is geometrically equivalent to reversing the
    /// given vector and adding those.
    fn sub(self, vec: Vector) -> Vector {
        // self.add(vec.mul(-1.0))
        Vector {
            x: self.x - vec.x,
            y: self.y - vec.y,
            z: self.z - vec.z,
        }
    }
}

impl Mul<f64> for Vector {
    type Output = Vector;

    /// Returns a new vector that is the result of scalar multiplication of self.
    fn mul(self, scalar: f64) -> Vector {
        Vector {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.y * scalar,
        }
    }
}

impl Div<f64> for Vector {
    type Output = Vector;

    /// Returns a new vector that is the result of scalar division. In particular this is
    /// mathematically equivalent to scalar multiplication of the reciprocal.
    fn div(self, scalar: f64) -> Vector {
        self.mul(1.0 / scalar)
    }
}

impl Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Vector {
        Vector {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

/// Performs a dot-product (a.k.a. the inner product) of two vectors.
/// There are many equalities connected to this. See
/// <https://en.wikipedia.org/wiki/Dot_product> for a bunch of information.
pub fn dot(a: Vector, b: Vector) -> f64 {
    (a.x * b.x) + (a.y * b.y) + (a.z * b.z)
}

/// Performs a cross-product of two vectors. If memory serves correctly, the idea is that
/// this produces a new vector that is orthogonal to the two given vectors, and with a
/// magnitude equal to the product of both of their magnitudes.
/// See <https://en.wikipedia.org/wiki/Cross_product> for more.
pub fn cross(a: Vector, b: Vector) -> Vector {
    Vector::new(
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Point;

    /// Page 4: we need to be able to construct vectors
    #[test]
    fn test_vector_factory() {
        let v = Vector::new(4.0, -4.0, 3.0);

        assert_eq!(v.x, 4.0);
        assert_eq!(v.y, -4.0);
        assert_eq!(v.z, 3.0);
    }

    /// Page 6: we need vector addition
    #[test]
    fn test_vector_addition() {
        let v1 = Vector::new(3.0, -2.0, 5.0);
        let v2 = Vector::new(-2.0, 3.0, 1.0);
        let v3 = v1.add(v2);

        assert_eq!(v3.x, 1.0);
        assert_eq!(v3.y, 1.0);
        assert_eq!(v3.z, 6.0);

        // and this way lets us test partialeq
        assert_eq!(v3, Vector::new(1.0, 1.0, 6.0));
    }

    /// Page 6: we need to be able to add vectors to points
    #[test]
    fn test_vector_point_addition() {
        let p1 = Point::new(3.0, -2.0, 5.0);
        let v = Vector::new(-2.0, 3.0, 1.0);
        let p2 = p1.add(v);

        assert_eq!(p2.x, 1.0);
        assert_eq!(p2.y, 1.0);
        assert_eq!(p2.z, 6.0);
    }

    /// Page 6: we need to be able to subtract vectors from points
    #[test]
    fn test_vector_point_subtraction() {
        let p1 = Point::new(3.0, 2.0, 1.0);
        let v = Vector::new(5.0, 6.0, 7.0);
        let p2 = p1.sub(v);

        assert_eq!(p2.x, -2.0);
        assert_eq!(p2.y, -4.0);
        assert_eq!(p2.z, -6.0);
    }

    /// Page 7: we need vector subtraction
    #[test]
    fn test_vector_subtraction() {
        let v1 = Vector::new(3.0, 2.0, 1.0);
        let v2 = Vector::new(5.0, 6.0, 7.0);
        let v3 = v1.sub(v2);

        assert_eq!(v3.x, -2.0);
        assert_eq!(v3.y, -4.0);
        assert_eq!(v3.z, -6.0);
    }

    #[test]
    fn test_vector_negation() {
        let v1 = Vector::new(3.0, 2.0, 1.0);
        assert_eq!(-v1, Vector::new(-3.0, -2.0, -1.0));
    }

    #[test]
    fn test_scalar_operations() {
        let v1 = Vector::new(1.0, 1.0, 1.0);
        // the operators are overloaded, but only work if the literal is on the right-hand
        // side
        assert_eq!(v1 * 10.0, Vector::new(10.0, 10.0, 10.0));
        assert_eq!(v1 / 10.0, Vector::new(0.1, 0.1, 0.1));
        assert_eq!(v1 * 0.1, v1 / 10.0);
    }

    #[test]
    fn test_vector_length() {
        let v1 = Vector::new(1.0, 1.0, 1.0);
        assert_eq!(eq_with_tolerance(v1.magnitude(), (3.0_f64).sqrt()), true);
    }

    #[test]
    fn test_vector_normalization() {
        assert_eq!(Vector::new(10.0, 0.0, 0.0).normalized(), Vector::UNIT_X);
        assert_eq!(Vector::new(0.0, 10.0, 0.0).normalized(), Vector::UNIT_Y);
        assert_eq!(Vector::new(0.0, 0.0, 10.0).normalized(), Vector::UNIT_Z);
    }

    #[test]
    fn test_vector_dot_products() {
        let v1 = Vector::new(1.0, 2.0, 3.0);
        let v2 = Vector::new(2.0, 3.0, 4.0);
        assert_eq!(dot(v1, v2), 20.0);

        // an important equality is that the dot product of a vector with itself is the
        // square of the magnitude
        let v3 = Vector::new(4.0, 5.0, 6.0);
        assert_eq!(eq_with_tolerance(dot(v3, v3), v3.magnitude().powi(2)), true);
    }

    #[test]
    fn test_vector_cross_products() {
        let v1 = Vector::new(1.0, 2.0, 3.0);
        let v2 = Vector::new(2.0, 3.0, 4.0);
        assert_eq!(cross(v1, v2), Vector::new(-1.0, 2.0, -1.0));
    }
}
